#include "llvm/Support/Host.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"
#include "llvm/ADT/ilist.h"
#include "llvm/ADT/MapVector.h"

#include <list>
#include <unordered_map>
#include <algorithm>

using namespace llvm;
using namespace std;

typedef list<Instruction*> InstList;
typedef unordered_map<BasicBlock, InstList> BasicBlockInstListMap;
enum direction_t {
    forward,
    backward
};

bool isEntry(BasicBlock &bb, Function &function, direction_t d) {
    if (d == direction_t::forward) {
        return bb.getName() == function.front().getName();
    } else {
        return bb.getName() == function.back().getName();
    }
}

void generalizedIterativeAlgorithm(
        Function &function,
        direction_t dir,
        InstList & (*transferFunction)(BasicBlock&, InstList&),
        InstList boundaryValue,
        InstList initialValue,
        InstList & (*lattice)(InstList&, InstList&),
        BasicBlockInstListMap &in,
        BasicBlockInstListMap &out)
{
    // tmpIn is the set which is calculated using the lattice
    BasicBlockInstListMap &tmpIn = (dir == direction_t::forward) ? in : out;
    // tmpOut is the set which is calculated using transfer function
    BasicBlockInstListMap &tmpOut = (dir == direction_t::forward) ? out : in;
    for (BasicBlock &bb : function) {
        if (isEntry(bb, function, dir))
            tmpOut[bb] = boundaryValue;
        else
            tmpOut[bb] = initialValue;
    }
    bool outHasChanged = true;
    while(outHasChanged) {
        outHasChanged = false;
        for (BasicBlock &bb : function) {
            if (isEntry(bb, function, dir))
                continue;
            InstList curIn = initialValue;
            for (pred_iterator i = pred_begin(&bb); i != pred_end(&bb); ++i) {
                BasicBlock *pred = *i;
                curIn = lattice(tmpOut[*pred], curIn);
            }
            tmpIn[bb] = curIn;
            unsigned oldSize = tmpOut.size();
            tmpOut[bb] = transferFunction(bb, tmpIn[bb]);
            outHasChanged |= oldSize != tmpOut.size();
        }
    }
}

int main(int argc, char **argv) {
    if (argc < 2) {
        errs() << "Need file as argument\n";
        return 1;
    }

    SMDiagnostic Err;
    std::unique_ptr<Module> Mod(ParseIRFile(argv[1], Err, getGlobalContext()));
    if (!Mod) {
        Err.print(argv[0], errs());
        return 1;
    }
    
    // iterate over functions
    for(Module::iterator F = Mod->begin(), E = Mod->end(); F!= E; ++F)
    {

        outs()<<"Function:"<<F->getName()<<"\n";
        int cur_count = -1;

        // vector of basic blocks integer names
        vector<int> basic_blocks;
        
        // iterate over basic blocks of current function
        for (iplist<BasicBlock>::iterator iter = F->getBasicBlockList().begin();
                    iter != F->getBasicBlockList().end();
                    iter++)
        {
            BasicBlock* currBB = iter;
            cur_count++;
            
            // set name of current basic block
            std::string s = std::to_string(cur_count);
            const char *cur_name = s.c_str(); 
            currBB->setName(string(cur_name));
            
            basic_blocks.push_back(cur_count);
            
            outs() << "BasicBlock: B"  << currBB->getName() << "\n"; 
                       
            // iterate over instructions of current basic block
            for (BasicBlock::iterator i = currBB->begin(), e = currBB->end(); i != e; ++i) {
               Instruction* inst = i;
               outs() << *inst << "\n";
            }    
        }
        
        int number_of_BB = basic_blocks.size();
        outs() << "\nNumber_of_Basic blocks: " << number_of_BB << "\n\n";
        
        // pred_vec - vector that contains Pred of all basic block i-th
        // component correspond to i-th basic block
        // succ_vec - similarly Succ
        vector< vector<int> > pred_vec, succ_vec;
        
        // initialize pred_vec and succ_vec uses pred_iterator and succ_iterator
        for (iplist<BasicBlock>::iterator iter = F->getBasicBlockList().begin();
                    iter != F->getBasicBlockList().end();
                    iter++)
        {
            BasicBlock* currBB = iter;
            
            vector<int> cur_pred;
            for (pred_iterator PI = pred_begin(currBB), E = pred_end(currBB); PI != E; ++PI) {
                 BasicBlock *Pred = *PI;
                 string name = Pred->getName();
                 int k = atoi(name.c_str());
                 cur_pred.push_back(k);
            }
            pred_vec.push_back(cur_pred);
            
            vector<int> cur_succ;
            for (succ_iterator PI = succ_begin(currBB), E = succ_end(currBB); PI != E; ++PI) {
                 BasicBlock *Succ = *PI;
                 string name = Succ->getName();
                 int k = atoi(name.c_str());
                 cur_succ.push_back(k);
            }
            succ_vec.push_back(cur_succ);
        }
        
        // vector of reverse_arcs for current function CFG
        vector<std::pair<int, int>> reverse_arcs;
        
        // finding all reverse arcs for current function CFG
        for (int i=0; i<pred_vec.size(); i++) {
            outs() << "BasicBlock: B"  << basic_blocks[i] << "\n"; 
            outs() << "Pred: ";
            for (int j=0; j<pred_vec[i].size(); j++) {
                outs() << "B" << pred_vec[i][j] << " ";
                if ( basic_blocks[i] < pred_vec[i][j] ) {
                    reverse_arcs.push_back( std::make_pair(pred_vec[i][j],basic_blocks[i]) );
                }
            }
            outs() << "\nSucc: ";
            for (int j=0; j<succ_vec[i].size(); j++) {
                outs() << "B" << succ_vec[i][j] << " ";
            }
            outs() << "\n";
        }
    }

  return 0;
}

